﻿using RajatPatwari.GridWorld.Occupant;
using System;
using System.Windows;
using System.Windows.Media;

namespace RajatPatwari.GridWorld.Example
{
    public static class Program
    {
        [STAThread]
        public static void Main()
        {
            try
            {
                World world = null;
                world = new World(10, 10,
                    handler: (sender, e) => MessageBox.Show(world.ToString(), "String Representation of GridWorld",
                        MessageBoxButton.OK, MessageBoxImage.Information));
                world.Add(new Bug());
                world.Add(new Bug { Color = Colors.DarkGoldenrod, LeaveFlower = false });
                world.Add(new Critter { Direction = Directions.Southwest + Angles.Left });
                world.Add(new Rock(), new Location(3, 6) / 3 + Locations.Row);
                world.Add(new Flower { Color = ColorUtilities.GenerateRandomColor(), DarkeningFactor = 0.1 });
                world.Run();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }
            finally
            {
                MessageBox.Show("Thank you for trying out GridWorld in C#! "
                    + "Once again, this is a replica of the original GridWorld Java Case Study and in no way reflects the quality of College Board's work."
                    + Environment.NewLine + "By: Rajat Patwari", "Thank You!", MessageBoxButton.OK,
                    MessageBoxImage.Information);
            }
        }
    }
}