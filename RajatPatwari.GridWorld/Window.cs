﻿using RajatPatwari.GridWorld.Occupant;
using System;
using System.Collections.Generic;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace RajatPatwari.GridWorld
{
    public sealed class Window : System.Windows.Window
    {
        private readonly int _rows, _columns;

        private readonly Button _step;

        private Canvas? _canvas;

        public Window(int rows, int columns, double height, double width, RoutedEventHandler handler)
        {
            Title = "GridWorld";
            Icon = new BitmapImage(new Uri(FileUtilities.GetPath(Title)));

            _rows = rows;
            _columns = columns;

            Height = height;
            Width = width;

            _canvas = new Canvas();

            _step = new Button { Content = "Step", Height = 20, Width = 30 };
            _step.Click += handler;
            Panel.SetZIndex(_step, 2);
            _canvas.Children.Add(_step);

            Content = _canvas;
        }

        public (double height, double width) GetCellSize() =>
            ((Height - 35) / _rows, (Width - 10) / _columns);

        public (double x, double y) GetPositionFromLocation(Location location)
        {
            var (height, width) = GetCellSize();
            return (location.Column * width + width / 2, location.Row * height + height / 2);
        }

        public void Clear()
        {
            if (_canvas == null)
                throw new InvalidOperationException(nameof(_canvas));

            Content = default;
            _canvas.Children.Remove(_step);
            _canvas = default;
        }

        public void DrawGrid()
        {
            _canvas = new Canvas();
            _canvas.Children.Add(_step);

            var (height, width) = GetCellSize();

            for (var i = 0; i < _rows; i++)
                for (var j = 0; j < _columns; j++)
                {
                    var line = new Line
                    {
                        X1 = j * width,
                        X2 = j * width,
                        Y1 = i * height,
                        Y2 = (i + 1) * height,
                        Stroke = Brushes.Black,
                        StrokeThickness = 1
                    };
                    _canvas.Children.Add(line);
                }

            for (var i = 0; i < _columns; i++)
                for (var j = 0; j < _rows; j++)
                {
                    var line = new Line
                    {
                        X1 = i * width,
                        X2 = (i + 1) * width,
                        Y1 = j * height,
                        Y2 = j * height,
                        Stroke = Brushes.Black,
                        StrokeThickness = 1
                    };
                    _canvas.Children.Add(line);
                }

            Content = _canvas;
        }

        public void DrawActor(Actor actor)
        {
            if (_canvas == null)
                throw new InvalidOperationException(nameof(_canvas));
            if (actor == null)
                throw new ArgumentNullException(nameof(actor));

            var (x, y) = GetPositionFromLocation(actor.Location ?? throw new InvalidOperationException(nameof(actor)));
            var rotation = new RotateTransform(actor.Direction);

            const double sizeMultiplier = 0.375;
            var imageSource = FileUtilities.GetPath(actor.GetType());
            var (cellHeight, cellWidth) = GetCellSize();
            double height = cellHeight * sizeMultiplier, width = cellWidth * sizeMultiplier;

            var bitmapImage = new BitmapImage();
            bitmapImage.BeginInit();
            bitmapImage.UriSource = new Uri(imageSource);
            bitmapImage.DecodePixelHeight = (int)height;
            bitmapImage.DecodePixelWidth = (int)width;
            bitmapImage.EndInit();

            FormatConvertedBitmap? newBitmap = null;

            if (actor.Color.A == byte.MaxValue)
            {
                newBitmap = new FormatConvertedBitmap();
                newBitmap.BeginInit();
                newBitmap.Source = bitmapImage;

                var colors = new List<Color> { Colors.White, actor.Color };
                var palette = new BitmapPalette(colors);

                newBitmap.DestinationPalette = palette;
                newBitmap.DestinationFormat = PixelFormats.Indexed1;
                newBitmap.EndInit();
            }

            var image = new Image
            {
                Source = (ImageSource)newBitmap ?? bitmapImage,
                Height = height,
                Width = width,
                RenderTransform = rotation
            };

            Canvas.SetLeft(image, x);
            Canvas.SetTop(image, y);

            _canvas.Children.Add(image);
        }
    }
}