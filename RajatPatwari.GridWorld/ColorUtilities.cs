﻿using System;
using System.Windows.Media;

namespace RajatPatwari.GridWorld
{
    public static class ColorUtilities
    {
        private static readonly Random random = new Random();

        public static Color Darken(Color color, double factor)
        {
            if (factor < 0 || factor > 1)
                throw new ArgumentOutOfRangeException(nameof(factor));

            var adjustedFactor = 1 - factor;
            return Color.FromArgb(color.A, (byte)(color.R * adjustedFactor), (byte)(color.G * adjustedFactor),
                (byte)(color.B * adjustedFactor));
        }

        public static Color GenerateRandomColor() =>
            Color.FromRgb((byte)random.Next(0, 256), (byte)random.Next(0, 256), (byte)random.Next(0, 256));
    }
}