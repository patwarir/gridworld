﻿using RajatPatwari.GridWorld.Occupant;
using System;
using System.Collections.Generic;
using System.Windows;

namespace RajatPatwari.GridWorld
{
    public sealed class World
    {
        private static readonly Random random = new Random();

        private readonly Grid _grid;

        private readonly Window _window;

        public World(int rows, int columns, double height = 600, double width = 800, RoutedEventHandler? handler = null)
        {
            _grid = new Grid(rows, columns);
            _window = new Window(rows, columns, height, width, handler == null ? Step : Step + handler);
        }

        public void Add(Actor actor)
        {
            if (actor == null)
                throw new ArgumentNullException(nameof(actor));

            Location location;
            do
            {
                location = new Location(random.Next(0, _grid.Rows), random.Next(0, _grid.Columns));
            } while (_grid.Get(location) != null);

            Add(actor, location);
        }

        public void Add(Actor actor, Location location) =>
            (actor ?? throw new ArgumentNullException(nameof(actor))).PutSelfInGrid(_grid, location);

        private void DrawActors()
        {
            _window.Show();
            _window.DrawGrid();

            foreach (var occupiedLocation in _grid.GetOccupiedLocations())
                _window.DrawActor(_grid.Get(occupiedLocation));
        }

        private void Act()
        {
            var actors = new List<Actor>();
            foreach (var occupiedLocation in _grid.GetOccupiedLocations())
                actors.Add(_grid.Get(occupiedLocation));
            foreach (var actor in actors)
                actor.Act();
        }

        private void Show()
        {
            _window.Clear();
            DrawActors();
        }

        private void Step(object sender, RoutedEventArgs e)
        {
            _window.Clear();
            Act();
            DrawActors();
        }

        public void Run()
        {
            Show();

            var application = new Application { MainWindow = _window };
            application.Run(_window);
        }

        public override string ToString() =>
            _grid.ToString();
    }
}