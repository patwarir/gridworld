﻿using RajatPatwari.GridWorld.Occupant;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RajatPatwari.GridWorld
{
    public sealed class Grid
    {
        private readonly Actor?[,] _actors;

        public Grid(int rows, int columns) =>
            _actors = new Actor?[rows, columns];

        public int Rows =>
            _actors.GetLength(0);

        public int Columns =>
            _actors.GetLength(1);

        public bool IsValid(Location? location) =>
            location.HasValue && 0 <= location.Value.Row && location.Value.Row < Rows && 0 <= location.Value.Column
            && location.Value.Column < Columns;

        public Actor? Get(Location location) =>
            _actors[location.Row, location.Column];

        public void Remove(Location location) =>
            _actors[location.Row, location.Column] = default;

        public void Put(Location location, Actor actor) =>
            _actors[location.Row, location.Column] = actor;

        public IEnumerable<Location> GetOccupiedLocations()
        {
            var occupiedLocations = new List<Location>();
            for (var row = 0; row < Rows; row++)
                for (var column = 0; column < Columns; column++)
                {
                    var location = new Location(row, column);
                    if (Get(location) != null)
                        occupiedLocations.Add(location);
                }

            return occupiedLocations;
        }

        public IEnumerable<Location> GetValidAdjacentLocations(Location location)
        {
            var validAdjacentLocations = new List<Location>();
            var direction = Directions.North;
            for (var i = 0; i < Angles.Circle / Angles.HalfRight; i++, direction += Angles.HalfRight)
            {
                var adjacentLocation = location.GetAdjacentLocation(direction);
                if (IsValid(adjacentLocation))
                    validAdjacentLocations.Add(adjacentLocation);
            }

            return validAdjacentLocations;
        }

        public IEnumerable<Location> GetEmptyAdjacentLocations(Location location) =>
            GetValidAdjacentLocations(location).Where(loc => Get(loc) == null);

        public IEnumerable<Location> GetOccupiedAdjacentLocations(Location location) =>
            GetValidAdjacentLocations(location).Where(loc => Get(loc) != null);

        public IEnumerable<Actor> GetNeighbors(Location location) =>
            GetOccupiedAdjacentLocations(location).Select(Get);

        public override string ToString()
        {
            var builder = new StringBuilder("{");
            foreach (var occupiedLocation in GetOccupiedLocations())
            {
                if (builder.Length > 1)
                    builder.Append(';');
                builder.Append(Get(occupiedLocation));
            }

            return builder.Append('}').ToString();
        }
    }
}