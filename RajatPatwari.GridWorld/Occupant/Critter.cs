﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Media;

namespace RajatPatwari.GridWorld.Occupant
{
    public class Critter : Actor
    {
        private static readonly Random random = new Random();

        static Critter() =>
            DefaultColor = Colors.Blue;

        public virtual IEnumerable<Actor> GetOccupants()
        {
            if (Grid == null)
                throw new InvalidOperationException(nameof(Grid));
            if (Location == null)
                throw new InvalidOperationException(nameof(Location));

            return Grid.GetNeighbors(Location.Value);
        }

        public virtual void ProcessOccupants(ref IEnumerable<Actor> occupants)
        {
            if (occupants == null)
                throw new ArgumentNullException(nameof(occupants));

            foreach (var occupant in occupants)
                if (occupant != null && !(occupant is Flower || occupant is Rock))
                    occupant.RemoveSelfFromGrid();
        }

        public virtual IEnumerable<Location> GetMoveLocations()
        {
            if (Grid == null)
                throw new InvalidOperationException(nameof(Grid));
            if (Location == null)
                throw new InvalidOperationException(nameof(Location));

            return Grid.GetEmptyAdjacentLocations(Location.Value);
        }

        public virtual Location SelectMoveLocation(IEnumerable<Location> locations)
        {
            if (locations == null)
                throw new ArgumentNullException(nameof(locations));

            var list = locations.ToList();
            return list.ElementAt(random.Next(list.Count));
        }

        public virtual void MakeMove(Location location) =>
            MoveTo(location);

        public sealed override void Act()
        {
            var occupants = GetOccupants();
            ProcessOccupants(ref occupants);

            var locations = GetMoveLocations();
            var location = SelectMoveLocation(locations);
            MakeMove(location);
        }
    }
}