﻿using System;
using System.Windows.Media;

namespace RajatPatwari.GridWorld.Occupant
{
    public abstract class Actor
    {
        protected static Color DefaultColor { get; set; }

        public Grid? Grid { get; private set; }

        public Location? Location { get; private set; }

        public Color Color { get; set; } = DefaultColor;

        private double _direction = Directions.North;

        public double Direction
        {
            get => _direction;
            set => _direction = AngleUtilities.Normalize(value);
        }

        static Actor() =>
            DefaultColor = Colors.Blue;

        public void PutSelfInGrid(Grid grid, Location location)
        {
            if (Grid != null)
                throw new InvalidOperationException(nameof(Grid));
            if (Location != null)
                throw new InvalidOperationException(nameof(Location));
            if (grid == null)
                throw new ArgumentNullException(nameof(grid));
            if (!grid.IsValid(location))
                throw new InvalidOperationException(nameof(location));

            grid.Get(location)?.RemoveSelfFromGrid();
            grid.Put(location, this);

            Grid = grid;
            Location = location;
        }

        public void RemoveSelfFromGrid()
        {
            if (Grid == null)
                throw new InvalidOperationException(nameof(Grid));
            if (Location == null)
                throw new InvalidOperationException(nameof(Location));

            Grid.Remove(Location.Value);

            Grid = default;
            Location = default;
        }

        public void MoveTo(Location location)
        {
            if (Grid == null)
                throw new InvalidOperationException(nameof(Grid));
            if (Location == null)
                throw new InvalidOperationException(nameof(Location));
            if (!Grid.IsValid(location))
                throw new InvalidOperationException(nameof(location));
            if (location == Location.Value)
                return;

            Grid.Remove(Location.Value);
            Grid.Get(location)?.RemoveSelfFromGrid();

            Location = location;
            Grid.Put(location, this);
        }

        public abstract void Act();

        public sealed override string ToString() =>
            $"{GetType().Name}[{Location},{Direction.ToAngleString()},{Color}]";
    }
}