﻿using System;
using System.Windows.Media;

namespace RajatPatwari.GridWorld.Occupant
{
    public class Bug : Actor
    {
        public bool LeaveFlower { get; set; } = true;

        static Bug() =>
            DefaultColor = Colors.Red;

        public bool CanMove()
        {
            if (Grid == null)
                return false;
            if (Location == null)
                return false;

            var next = Location.Value.GetAdjacentLocation(Direction);
            if (!Grid.IsValid(next))
                return false;

            var occupant = Grid.Get(next);
            return occupant == null || occupant is Flower;
        }

        public void Move()
        {
            if (Grid == null)
                throw new InvalidOperationException(nameof(Grid));
            if (Location == null)
                throw new InvalidOperationException(nameof(Location));

            var location = Location.Value;
            var next = location.GetAdjacentLocation(Direction);

            if (Grid.IsValid(next))
                MoveTo(next);
            else
                RemoveSelfFromGrid();

            if (LeaveFlower)
            {
                var flower = new Flower { Color = Color };
                flower.PutSelfInGrid(Grid, location);
            }
        }

        public void Turn() =>
            Direction += Angles.HalfRight;

        public override void Act()
        {
            if (CanMove())
                Move();
            else
                Turn();
        }
    }
}