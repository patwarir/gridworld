﻿using System.Windows.Media;

namespace RajatPatwari.GridWorld.Occupant
{
    public class Flower : Actor
    {
        public double DarkeningFactor { get; set; } = 0.05;

        static Flower() =>
            DefaultColor = Colors.DarkOrange;

        public override void Act() =>
            Color = ColorUtilities.Darken(Color, DarkeningFactor);
    }
}