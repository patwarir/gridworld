﻿using System;

namespace RajatPatwari.GridWorld
{
    public static class Angles
    {
        public const double
            Straight = 0,
            HalfLeft = -45,
            HalfRight = 45,
            Left = -90,
            Right = 90,
            HalfCircle = 180,
            Circle = 360;
    }

    public static class Directions
    {
        public const double
            North = 0,
            Northeast = 45,
            East = 90,
            Southeast = 135,
            South = 180,
            Southwest = 225,
            West = 270,
            Northwest = 315;
    }

    public static class AngleUtilities
    {
        public static double Normalize(double angle)
        {
            angle %= Angles.Circle;
            while (angle < Angles.Straight)
                angle += Angles.Circle;
            return (int)angle / (int)Angles.HalfRight * (int)Angles.HalfRight;
        }

        public static double ToDegrees(double radians) =>
            radians / Math.PI * 180;

        public static double ToRadians(double degrees) =>
            degrees / 180 * Math.PI;

        public static string ToAngleString(this double degrees) =>
            $"{degrees}°";
    }
}