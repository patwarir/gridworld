﻿using System;
using System.IO;

namespace RajatPatwari.GridWorld
{
    public static class FileUtilities
    {
        private static readonly string resourceDirectory = Path.Combine(Environment.CurrentDirectory, "Resources");

        public static string GetPath(string name, string extension = "gif")
        {
            if (string.IsNullOrWhiteSpace(name))
                throw new InvalidOperationException(nameof(name));
            if (string.IsNullOrWhiteSpace(extension))
                throw new InvalidOperationException(nameof(extension));

            var path = Path.Combine(resourceDirectory, $"{name}.{extension}");
            return File.Exists(path)
                ? path
                : throw new InvalidOperationException($"Cannot find image for: {name}.{extension}!");
        }

        public static string GetPath(Type type, string extension = "gif")
        {
            if (type == null)
                throw new ArgumentNullException(nameof(type));
            if (string.IsNullOrWhiteSpace(extension))
                throw new InvalidOperationException(nameof(extension));

            var typeName = type.Name;
            while (type != typeof(object))
            {
                var path = Path.Combine(resourceDirectory, $"{type.Name}.{extension}");
                if (File.Exists(path))
                    return path;
                type = type.BaseType;
            }

            throw new InvalidOperationException($"Cannot find image for: {typeName}.{extension}!");
        }
    }
}