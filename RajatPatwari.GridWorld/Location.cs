﻿using System;

namespace RajatPatwari.GridWorld
{
    public static class Locations
    {
        public static readonly Location
            Zero = new Location(0, 0),
            Row = new Location(1, 0),
            Column = new Location(0, 1),
            One = new Location(1, 1);
    }

    public readonly struct Location : ICloneable, IEquatable<Location>, IComparable, IComparable<Location>
    {
        public int Row { get; }

        public int Column { get; }

        public Location(int row, int column)
        {
            Row = row;
            Column = column;
        }

        object ICloneable.Clone() =>
            new Location(Row, Column);

        public Location GetAdjacentLocation(double direction)
        {
            var (deltaRow, deltaColumn) = AngleUtilities.Normalize(direction) switch
            {
                Directions.North => (-1, 0),
                Directions.Northeast => (-1, 1),
                Directions.East => (0, 1),
                Directions.Southeast => (1, 1),
                Directions.South => (1, 0),
                Directions.Southwest => (1, -1),
                Directions.West => (0, -1),
                Directions.Northwest => (-1, -1),
                _ => (0, 0)
            };
            return new Location(Row + deltaRow, Column + deltaColumn);
        }

        public double GetDirectionToward(Location location) =>
            AngleUtilities.Normalize(Angles.Right
                - AngleUtilities.ToDegrees(Math.Atan2(Row - location.Row, location.Column - Column)));

        public double GetDistanceTo(Location location) =>
            Math.Sqrt(Math.Pow(location.Row - Row, 2) + Math.Pow(location.Column - Column, 2));

        public bool Equals(Location location) =>
            Row == location.Row && Column == location.Column;

        public override bool Equals(object? obj) =>
            obj is Location location ? Equals(location) : throw new ArgumentException(nameof(obj));

        public static bool operator ==(Location location1, Location location2) =>
            location1.Equals(location2);

        public static bool operator !=(Location location1, Location location2) =>
            !location1.Equals(location2);

        public override int GetHashCode() =>
            HashCode.Combine(Row, Column);

        public int CompareTo(Location location)
        {
            if (Equals(location))
                return 0;

            if (Row > location.Row && Column > location.Column)
                return 1;
            if (Row < location.Row && Column < location.Column)
                return -1;

            if (Row > location.Row)
                return 1;
            if (Row < location.Row)
                return -1;

            if (Column > location.Column)
                return 1;
            if (Column < location.Column)
                return -1;

            throw new ArgumentException(nameof(location));
        }

        public int CompareTo(object? obj) =>
            obj is Location location ? CompareTo(location) : throw new ArgumentException(nameof(obj));

        public static bool operator >(Location location1, Location location2) =>
            location1.CompareTo(location2) > 0;

        public static bool operator <(Location location1, Location location2) =>
            location1.CompareTo(location2) < 0;

        public static bool operator >=(Location location1, Location location2) =>
            location1.CompareTo(location2) >= 0;

        public static bool operator <=(Location location1, Location location2) =>
            location1.CompareTo(location2) <= 0;

        public static Location operator +(Location location) =>
            new Location(location.Row, location.Column);

        public static Location operator -(Location location) =>
            new Location(-location.Row, -location.Column);

        public static Location operator +(Location location1, Location location2) =>
            new Location(location1.Row + location2.Row, location1.Column + location2.Column);

        public static Location operator -(Location location1, Location location2) =>
            new Location(location1.Row - location2.Row, location1.Column - location2.Column);

        public static Location operator *(Location location, int factor) =>
            new Location(location.Row * factor, location.Column * factor);

        public static Location operator /(Location location, int factor) =>
            new Location(location.Row / factor, location.Column / factor);

        public static Location operator %(Location location, int factor) =>
            new Location(location.Row % factor, location.Column % factor);

        public static explicit operator Location((int row, int column) location) =>
            new Location(location.row, location.column);

        public static explicit operator (int row, int column)(Location location) =>
            (location.Row, location.Column);

        public override string ToString() =>
            $"({Row}|{Column})";
    }
}