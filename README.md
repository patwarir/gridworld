# RajatPatwari.GridWorld
A _copy_ of the AP Computer Science A GridWorld case study written in C#.

### Disclaimer
I __do not own__, nor claim to own, any of the AP Computer Science A GridWorld case study materials.
This is simply a project that I made because I found GridWorld interesting.
All GridWorld case study materials are property of College Board.

This project is ___not___ the same as College Board's GridWorld case study; I have added my own spin on many things in the project.
It does not in any way reflect the quality of College Board's work.

### Students
Please _do not use this project for your AP Computer Science class' assignments!_
* It is written in C#, not Java.
* I have changed many things to reflect my own programming style, not College Board's.
* College Board does not require you to know the backend of GridWorld for the AP Computer Science exam.

Only use this project if you are curious on how a C# GridWorld replica would function.

### Notes
__Framework:__ .NET Core 3.1

__Author:__ Rajat Patwari

__License:__ BSD 3-Clause